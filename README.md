### 1.0.0
- mono ```version 4.8.1 (Stable 4.8.1.0/22a39d7 Wed Apr 12 12:00:40 UTC 2017)```
- Docker ```version 1.12.1, build 23cf638```
- docker-compose ```version 1.8.0, build f3628c7```
- dotnet ```1.0.4```
- node ```7.10.0```
- npm ```4.2.0```
- yarn ```0.23.4```
- git ```1.9.1```
